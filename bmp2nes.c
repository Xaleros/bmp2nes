// vim: set noai ts=2 sw=2:
/*---------------------------------------------------------------------------
	bmp2nes is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	bmp2nes.c: 	a program to convert 24-bit BMP files to NES character ROM
							data. Can also dump namespace tables for verification of
							character ROM.

	Written by Adam Smith
---------------------------------------------------------------------------*/

#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include "string.h"

#define BMP_HDR_SIZE 12

typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;

typedef struct tile_s
{
	u8 data[16];

} tile_t;

typedef struct bmp_hdr_s
{
	char ident[2];
	char _pad0[6];
	u32 filesize;
	u32 _unused;
	u32 offset;

} bmp_hdr_t;

typedef struct bmp_s
{
	u32 dib_size;
	int width;
	int height;
	u16 cplanes;
	u16 bpp;
	u32 compress;
	u32 size;
	u32 xppm;
	u32 yppm;
	u32 cnum;
	u32 cimp;

} bmp_t;

u32 encoded = 0;				// number of tiles that have been encoded
u16 chr_bank_size = 0;
u32 tiles_x = 0;	
u32 tiles_y = 0;

char *bmp_fn = NULL;
char *chr_fn = NULL;
char *ntb_fn = NULL;
int of_needs_free = 0;
int ntbf_needs_free = 0;

u32 colors[4];					// colors that are unique
u8  num_colors = 0;			// number of registered colors
tile_t *tilemap;				// map of all tile data
int gen_nametable = 0;	// if the user wants a nametable
u8  pal_swap[4];			  // if the user wants certain palette values swapped

void get_chr_fnname (void)
{
	size_t fnsz = strlen (bmp_fn);
	chr_fn = strdup (bmp_fn);
	chr_fn[fnsz - 3] = 'c';
	chr_fn[fnsz - 2] = 'h';
	chr_fn[fnsz - 1] = 'r';
	of_needs_free = 1;
}

void get_ntb_filename (void)
{
	size_t fnsz = strlen (bmp_fn);
	ntb_fn = strdup (bmp_fn);
	ntb_fn[fnsz - 3] = 'n';
	ntb_fn[fnsz - 2] = 'a';
	ntb_fn[fnsz - 1] = 'm';
	ntbf_needs_free = 1;
}

u32 *format_bmp_for_conv (bmp_t *bmp, u8 *data)
{
	size_t pixels = bmp->width * bmp->height;
	u32 *out = malloc (4 * pixels);
	for (int y = bmp->height-1; y >= 0; y--)
	{
		for (int x = 0; x < bmp->width; x++)
		{
			u32 b = ((u32)*data++) << 16;
			u32 g = ((u32)*data++) << 8;
			u32 r = ((u32)*data++);
			u32 coord = (y*bmp->width) + x;
			out[coord] = b | g | r;
		}
	}

	return out;
}

int get_pal_index (u32 pixel)
{
	if (num_colors == 0)
	{
		colors[num_colors] = pixel;
		return num_colors++;
	}

	// check if any of the colors are equal
	for (int i = 0; i < num_colors; i++)
	{
		if (pixel == colors[i])
			return i;
	}

	// new color
	if (num_colors == 4)
	{
		// find the color closest to it on the color spectrum
		float cdists[4];
		int closest = 0;
		for (int i = 0; i < num_colors; i++)
		{
			u32 r2 = pixel & 0xFFFFFF00;
			u32 r1 = colors[i] & 0xFFFFFF00;
			float rdist2 = pow (r2 - r1, 2); // (r2 - r1)^2
			
			u32 g2 = (pixel >> 8) & 0xFFFFFF00;
			u32 g1 = (colors[i] >> 8) & 0xFFFFFF00;
			float gdist2 = pow (g2 - g1, 2); // (g2 - g1)^2

			u32 b2 = pixel >> 16;
			u32 b1 = colors[i] >> 16;
			float bdist2 = pow (b2 - b1, 2); // (b2 - b1)^2

			cdists[i] = sqrt (rdist2 + gdist2 + bdist2);

			// determine the current closest color
			if (i == 0 || cdists[closest] > cdists[i])
				closest = i;
		}

		return closest;
	}
	else
	{
		// add it
		colors[num_colors] = pixel;
		return num_colors++;
	}
}

tile_t encode_tile (u8 *pixpal)
{
	u8 bidx = 0;
	tile_t tile;
	memset (&tile, 0, sizeof(tile));
	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			u8 idx = (i*8) + j;
			tile.data[bidx]     <<= 1;
			tile.data[bidx + 8] <<= 1;
			if (pixpal[idx] == 3)
			{
				tile.data[bidx] 		|= 0x01;
				tile.data[bidx + 8] |= 0x01;
			}
			else if (pixpal[idx] == 2)
			{
				tile.data[bidx + 8] |= 0x01;
			}
			else if (pixpal[idx] == 1)
			{
				tile.data[bidx] |= 0x01;
			}
		}
		bidx++;
	}
	return tile;
}

int chr_contains_tile (tile_t tile)
{
	for (int i = 0; i < encoded; i++)
	{
		if (memcmp (tile.data, tilemap[i].data, 16) == 0)
			return 1;
	}
	return 0;
}

void print_help()
{
  printf ("bmp2nes: approximates BMP files to NES PPU data\n");
	printf ("         creates a CHR file by default\n");
  printf ("usage: bmp2chr [OPTIONS] file.bmp\n\n");
  printf ("Options:\n");
	printf ("    -b num: specifies CHR bank size, fills the CHR file with zeros if there is extra space\n");
	printf ("    -c 0x00BBGGRR 0x00BBGGRR 0x00BBGGRR 0x00BBGGRR: presets the colors that are recognized in a palette\n");
	printf ("    -n file: creats a nametable file\n");
  printf ("    -o file: specifies a CHR filename, defaults to original filename (ie. file.chr)\n");
	printf ("    -p ####: replaces palette entries according to the numbers (-p 2023 replaces palette index 0 with 2 and 1 with 0\n");
  printf ("\n");
	exit (-1);
}

int main (int argc, char **argv)
{
	if (argc == 1)
		print_help ();

	// get args
	int i = 1;
	do
	{
		if (strncmp (argv[i], "-b", 2) == 0 && i < argc + 1)
			chr_bank_size = strtol (argv[++i], NULL, 10);

		else if (strncmp (argv[i], "-c", 2) == 0 && i < argc + 4)
		{
			colors[0] = strtol (argv[++i], NULL, 16);
			colors[1] = strtol (argv[++i], NULL, 16);
			colors[2] = strtol (argv[++i], NULL, 16);
			colors[3] = strtol (argv[++i], NULL, 16);
			num_colors = 4;
		}

		else if (strncmp (argv[i], "-o", 2) == 0 && i < argc + 1)
			chr_fn = get_full_path (argv[++i]);

		else if (strncmp (argv[i], "-n", 2) == 0)
			gen_nametable = 1;

		else if (strncmp (argv[i], "-p", 2) == 0 && i < argc + 1)
		{
			char *swap = argv[++i];
			int j = 0;
			do
			{
				pal_swap[j] = swap[j] - 48;

			} while (++j < 4);
		}

		else if (bmp_fn == NULL)
			bmp_fn = get_full_path (argv[i]);

		else
			print_help ();

		i++;

	} while (i < argc);

	printf ("\n");

	// set defaults if needed
	if (chr_fn == NULL)
		get_chr_fnname ();

	// reserve the first color (has to be the same on all palettes and its generally black)
	colors[0] = 0;
	num_colors++;

	// load bmp file
	FILE *bmp_file = fopen (bmp_fn, "rb");
	if (!bmp_file)
	{
		printf ("input file '%s' does not exist\n\n", bmp_fn);
		exit (-2);
	}

	// check header
	bmp_hdr_t *hdr = malloc (sizeof(bmp_hdr_t));
	fread (hdr, 1, 2, bmp_file);

	if (strncmp (hdr->ident, "BM", 2) != 0)
	{
		printf ("file '%s' does not have a valid BMP identifier\n\n", bmp_fn);
		exit (-3);
	}

	// read rest of the header
	fread (&hdr->filesize, 1, BMP_HDR_SIZE, bmp_file);
	

	bmp_t *bmp = malloc (sizeof(bmp_t));

	// check dib size
	fread (&bmp->dib_size, 4, 1, bmp_file);
	if (bmp->dib_size != 40)
	{
		printf ("DIB size must be 40 bytes\n\n");
		exit (-4);
	}

	// go back
	fseek (bmp_file, -4, SEEK_CUR);

	// read entire dib
	fread (bmp, 40, 1, bmp_file);
	
	if (bmp->bpp != 24)
	{
		printf ("BMP must be 24 bits per pixel\n\n");
		exit (-5);
	}

	// verify dimensions are divisble by 8
	if (bmp->width % 8 != 0)
	{
		printf ("BMP width must be a multiple of 8\n\n");
		exit (-6);
	}
	if (bmp->height % 8 != 0)
	{
		printf ("BMP height must be a multiple of 8\n\n");
		exit (-6);
	}

	// set number of tiles
	tiles_x = bmp->width / 8;
	tiles_y = bmp->height / 8;
	if (gen_nametable && (tiles_x > 32 || tiles_y > 28))
	{
		printf ("warning: image size is too big to fit on 256x224 screen space\n");
		printf ("         not creating nametable.\n");
		gen_nametable = 0;
	}
	int num_tiles = tiles_x * tiles_y;

	// read pixels
	u32 data_size = 24 * bmp->width * bmp->height;
	u8 *raw_data = malloc (data_size);
	fseek (bmp_file, hdr->offset, SEEK_SET);
	fread (raw_data, 1, data_size, bmp_file);
	free (hdr);

	// reformat pixel data
	u32 *data = format_bmp_for_conv (bmp, raw_data);
	free (raw_data);

	// setup write to tile map
	u16 duplicates = 0;
	FILE *chr_file = fopen (chr_fn, "wb");	// fixme: add check for write permissions error
	tilemap = malloc (sizeof(tile_t) * num_tiles);
	
	// setup write to nametable file
	FILE *ntb_file = NULL;
	if (gen_nametable)
	{
		get_ntb_filename ();
		ntb_file = fopen (ntb_fn, "wb");
	}

	// for each tile...
	for (int i = 0; i < tiles_y; i++)
	{
		for (int j = 0; j < tiles_x; j++)
		{
			// for each pixel in the tile...
			u8 pixpal[64];
			u8 ppidx = 0;
			memset (pixpal, 0, 64);
			int ystart = i * 8;
			int yend = ystart + 8;
			for (int y = ystart; y < yend; y++)
			{
				int xstart = j * 8;
				int xend = xstart + 8;
				for (int x = xstart; x < xend; x++)
				{
					// calculate palette index
					u32 idx = (y*bmp->width) + x;
					pixpal[ppidx++] = get_pal_index (data[idx]);
				}
			}

			// encode the tile
			tile_t tile = encode_tile (pixpal);

			// ...but don't add it if the tile already exists
			if (!chr_contains_tile (tile))
				tilemap[encoded++] = tile;
			else
				duplicates++;

			// if needed, write to nametable file
			u8 tile_index = (i * 16) + j;
			if (gen_nametable)
				fwrite (&tile_index, 1, 1, ntb_file);
		}
		if (gen_nametable && tiles_x < 32)
		{
			u8 num_first_tile = 32 - tiles_x;
			u8 *zeros = malloc (num_first_tile);
			fwrite (zeros, 1, num_first_tile, ntb_file);
		}
	}

	// write attribute table if needed
	if (gen_nametable)
	{
		u8 *zeros = malloc (64);
		memset (zeros, 0, 64);
		fwrite (zeros, 1, 60, ntb_file);
		fclose (ntb_file);
	}

	// print statistics
	printf ("tile count: %i\n", num_tiles);
	printf ("duplicate count: %i\n", duplicates);
	printf ("unique tile count: %i\n", num_tiles - duplicates);

	// write chr file
	fwrite (tilemap, sizeof(tile_t), num_tiles, chr_file);
	if (chr_bank_size > 0)
	{
		int num_zeros = chr_bank_size - (num_tiles * 16);
  	u8 *zeros = malloc (num_zeros);
  	memset (zeros, 0, num_zeros);
		fwrite (zeros, 1, num_zeros, chr_file);
		free (zeros);
	}

	// cleanup
	fclose (chr_file);
	fclose (bmp_file);
	free (tilemap);
	free (data);
	free (bmp);
	if (of_needs_free)
		free (chr_fn);

	return 0;
}

